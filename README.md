# Hello EE World - Technical Challenge
Python application to present Hello EE World message and load balance the application within Minikube

## Python (helloworldweb.py)
Simple python application that outputs 'Hello and Welcome to the EE World' in a webpage.  The application imports and makes use of the web and socket modules.

## Dockerfile (dockerfile)
A configuration file to build a docker image that runs the python application. The docker image has been pushed up to the docker repository (itldocker/intersite)

The image was built as follows

To build and run the docker file run the following from the project folder.
* docker build -t python-hello-ee-world .
* docker run -p 8080:8080 python-hello-ee-world
  * Verify success execution of python application within docker container and that the [webpage](http://127.0.0.1:8080) is visible

Upload the docker image to the docker hub where the deployment.yaml file can pull the image from when minikube is applying the deployment.

## Minikube (deployment.yaml)
Allows the emulation of a Kubernetes configuration and load balances the application based on the configuration in the deployment.yaml file.

The current configuration is set to create 3 load balancers, to change this amend the following line and re-apply the deployment yaml file

>replicas: 3

## Usage
1. Navigate to the project folder

2. Ensure Minikube is running
* minikube start

3. Apply the configuration to start the application
* kubectl apply -f ./deployment.yaml

4. Get the NodePort address
* minikube service --url hello-world-service

**The url to view the application can be accessed on the nodeport address in your browser**

View the Minikube dashboard to verify the pods have been started successfully.
* minikube dashboard

or run the following to verify the services have been created and are running
* kubectl get pods
* kubectl get svc
* kubectl get deployment
