import web
import socket

urls = ("/.*", "hello")
app = web.application(urls, globals())

class hello:
    def GET(self):
        hstname = socket.gethostname()
        msg = 'Hello and Welcome to the EE World' + '\r\n\r\nYou\'ve connected to host: ' + hstname

        return msg

if __name__ == "__main__":
    app.run()